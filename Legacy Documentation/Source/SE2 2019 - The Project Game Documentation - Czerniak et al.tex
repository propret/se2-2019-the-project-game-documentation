\documentclass{article}

\usepackage[version=3]{mhchem} 
\usepackage{siunitx} 
\usepackage{graphicx} 
\usepackage{natbib} 
\usepackage{amsmath} 
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{xcolor}
\usepackage{float}
\usepackage{listings}
\usepackage{polski}
\usepackage{english}
\usepackage[utf8]{inputenc}
\usepackage{pdfpages}
\setlength\parindent{0pt} 
\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=true,
    frame=lines,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

\renewcommand{\labelenumi}{\alph{enumi}.}

\title{The Project Game \\ Documentation \\ Software Engineering}

\author{Kamil \textsc{Czerniak} \\ Kacper \textsc{Leszczyński} \\ Kuba \textsc{Ferliński} \\ Giorgi \textsc{Mamatsashvili} \\ Bartłomiej \textsc{Szymański}}

\date{February 26, 2019}

\begin{document}

\maketitle 

\begin{center}
\begin{tabular}{l r}
Instructors: & Aleksander Kosicki, Michał Okulewicz\\
Coordinator: & Agnieszka Jastrzębska
\end{tabular}
\end{center}
\vspace*{\fill}
\begin{center}
\Large{Version 1.0.0}
\end{center}

\newpage
\tableofcontents
\newpage
\section{Game overview}
\subsection{Introduction}
The goal of the Project Game is to simulate project development in an environment where cooperation within the team is required, as well as competition with other teams. The structure of the game reinforces three notions:

\begin{itemize}
    \item Every task carries a risk of failure.
    \item Goals of the project are not clear at start and they need to be discovered.
    \item Cooperation between team members speeds up delivery of the project.
\end{itemize}

Below, an example board is shown, with visual representations of what could appear on such a board during the game.
\begin{figure}[H]
    \includegraphics[width=\textwidth]{Board1.pdf}
    \caption{An example board}
    \label{fig:my_label1}
\end{figure}

In the game, players pick up pieces and place them in goal area. The risk of doing a task is simulated by a probability that a given piece is a sham, i.e. does not bring the project closer to completion. The player can discover that fact either by testing a piece they currently hold or by placing it in the goal area and not receiving any feedback on whether the piece is a part of a project goal or not.

The difficulty of project is simulated by the size of the goal area on board, the shape of the fields in which the pieces need to be placed and the number of those fields.

The cooperation between the players is simulated by the ability to exchange information about what they know about the state of the board and by having a common goal. The players can find out about project goals only by communicating with each other. That information can sometimes be old, which encourages close cooperation even further. 

Below, an example of how communication can help team members is shown. On the left, we see the state before members B and BP communicate - BP only sees things they have already discovered, such as where two R are and how far away they are from a piece P. On the right, we see the state after B and BP communicate - now BP knows where exactly the piece is. They also see an updated state of distances to that piece, which can improve their decision-making.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{Board2.pdf}
    \caption{Team member communication example}
    \label{fig:my_label2}
\end{figure}
As in many cases, a team needs a leader. Team leader gets a special role, as they can send messages to other team members without their prior approval (other players have to ask each other if they can send messages to one another). This allows a team leader to coordinate activities between team members. 
\subsection{Game guidelines}
  
\begin{itemize}
    \item The true state of the game is always known to the Game Master, but not the player.
   \item To find out about the true state of fields in the goal areas, the players have to place a piece in the field to get proper information.
   \item  When the piece is placed correctly, one of the goals is completed.
   \item  If the piece is placed incorrectly, the placement will not advance the players closer to reaching the goal.
   \item If the piece is a sham, the player gets no information. 
   \item The player can only move in 4 directions: up, bottom, left and right.
   \item The players can discover the contents of the 8 neighboring fields, check whether the piece is a sham, placing pieces in goals, exchange information with other players.
   \item  Player who moves into the field first gets the piece, the player cannot move into a field if it's occupied by another player.
   \item  After observing a field, the player has the information about the Manhattan distance to the nearest piece to them.
\end{itemize}
\newpage
\section{System Requirements}

\subsection{System requirement overview}

The system represents a game conducted involving teams of rival agents. It has three actors: Game Master, Board and Player. These actors communicate with each other using Communication Server. Game Master is responsible for managing the game, Board is responsible for holding the state of the game, while Players take part in the game. Players, by interacting with the Game Master, the Board and one another, seek to complete the game as fast as possible.

\subsection{Functional requirements}

\subsubsection{User stories}
\begin{itemize}
    \item As a user, I want to simulate a working environment so that I can cooperate better within a team.
    \item As a player, I want to cooperate with teammates to achieve a goal.
    \item As a player, I want to be able to move pieces, so that I can impact the game.
    \item As a player, I want to see results of my actions, so that I know how to act followingly.
    \item As a player, I want to control who can send messages to me.
    \item As a team leader, I want to be able to contact any team member whenever I need to, so that I can manage such team efficiently.
    \item As a game master, I want to know the state of the game, so that I can announce the winner or stop the game if it can't end.
    \item As a game master, I want to be able to set properties of the game, so that it can match my expectations.
    \item As a game master, I want to limit what players can do, so that the game is more challenging.
    
\end{itemize}

\subsubsection{Functionalities}

\begin{itemize}
	\item System should focus on two types of users, \textit{Gamemaster} and \textit{Player}. most of the system should consist of interaction between these two users.
	\item System should serve as an intermediary for communication between the clients.
	\item Game Master should generate the initial state of the board and all the goals at random.
	\item Game Master should hold the real state of the game at all times.
	\item Main activities are achieved through the communication between Game Master and Player
	\item Server can only be intermediary and is not functionally involved in the game.
	\item Many interactions from different point of views can be seen in the form of UML diagrams displayed in the later parts of the documentation. 
	\item Details of the implementation have not been specified and can be subject to change during the implementation phase.
	

\end{itemize}


\subsection{Non-functional requirements}

\begin{center}
	\begin{tabularx}{\textwidth}{|c|c|X|}
		\hline
		\multicolumn{1}{|c|}{\textbf{Category}} & \multicolumn{1}{c|}{\textbf{Number}} & \multicolumn{1}{c|}{\textbf{Description}} \\ \hline
		% Usability
		\multirow{4}{*}{\textbf{Usability}} & 1 &  All the communications between the two user types, Gamemaster and Player should be handled by the server. \\ \cline{2-3}
		& 2 & Gamemaster should be aware of and able to process all requests sent to him from Player. \\ \cline{2-3}
		& 3 & Gamemaster should make all the decisions locally according to the requests made to it.
		\\ \hline
		% Reliability
		\multirow{3}{*}{\textbf{Reliability}} & 4 & In case of a failure, user should be provided with an error message describing what exactly went wrong. 
		 \\ \hline
		% Performance
		\multirow{3}{*}{\textbf{Performance}} & 5 & System should work on all recent operating systems.
		 \\ \hline
		% Supportability
		\multirow{3}{*}{\textbf{Supportability}} & 6 & System and it's usability should be well-documented.   \\ \cline{2-3}
		& 7 & In this documentation should also be included a manual for the user.  \\ \cline{2-3}
		& 8 & To understand the role of each user in the game, use-case diagrams should be provided. 
		\\ \hline
	\end{tabularx}
\end{center}


\newpage
\section{Design Documentation}

\subsection{Architecture design}
In the system we have three parts Game Master, Player and Communications Server. Player represents the agent playing the game, the one with the view of the current state of the game. Game Master interacts with the game in an unbiased, regulatory manner. Its job is to generate the board and the goals, while keeping the information about the actual state of the game and generates new pieces. The Game Master also communicates with the Player what they can and cannot do and the Player must obey the instruction of the Game Master in order to participate in the game, in order for this to be true, the player is required to request the permission to perform actions from the Game Master before doing so. This communication between the Player and Game Master or between the Players themselves is allowed through the Communications Server whose responsibility it is to pass messages to appropriate recipients. 

\subsection{Communication protocol design}

Communication between systems will be facilitated using TCP, with messages formatted in JSON. The exact design of all messages will be decided collectively by the course group. Messages will have to be sent asynchronously and all players have to be open to messages from system at all times. 

\subsection{Special system states description}

In case of special states occurring for a given actor, the system will apply special behaviour.

For Game Masters, the system will be able to go into these special states:

\begin{itemize}
    \item when Game Master initializes the game, the system will respond to all messages from Players with a status message about initialization.
    \item when Game Master resets the game, the system sends a message to all connected players informing them of that fact. It then switches to initialization mode described above.
    \item when Game Master ends a game, the system sends a message to all connected players informing them of that fact. It then shuts down all connections to Communication Server. 
    \item when Game Master has lost connection to Communication Server, the game ends and all players are informed of that fact.
\end{itemize}

For players, the following special states have been distinguished:

\begin{itemize}
    \item when Player connects to Communication Server for first time and the game is being initialized, the system informs Game Master of that connection and which team was selected.
    \item when Player connects to Communication Server for first time and the game was already initialized, their connection is denied.
    \item when Player tries to communicate with a Player it didn't contact yet, the system sends a request to addressee of that request and the user has to respond to the request within a time threshold. If no response was sent, or there was a timeout, then this request is denied and continues to be until the end of the game. If the response was positive, the message is relayed and following messages are sent without request  (\textit{NOTE: Team Leader does not need to request communication - their messages are sent at all situations}).
    \item when Player receives an information about the result of the game, it closes down.
    \item when Player loses connection to Communication Server, the system waits for the user to reconnect.
\end{itemize}

For board, the system will use following special states:

\begin{itemize}
    \item when Board is being initialized, the system relays a subset of commands from Game Master to Board that relate to settings of Board (e.g. its size).
    \item when Board has lost connection to Communication Server, the game ends and all players are informed of that fact.
\end{itemize}

For Communication Server, the system will use following special states:

\begin{itemize}
    \item when Communication Server is initialized, it accepts connections from a single Board, a single Game Master and multiple Players until Game Master starts the game.
    \item when Communication Server loses connection to the Board or to the Game Master, it informs Players of that fact and closes down.
    \item when Communication Server loses connection to a Player, it accepts connections from that Player until successful connection is made.
\end{itemize}

\subsection{Game-related objects}

\subsubsection{Board}
Object meant for depicting the entire state of the current game. This object will store all the relevant information about what has transpired in the game as well as the ongoing process. With relevant methods, the board will be updated once some sort of interaction happens in the game.

\subsubsection{Team}
There can be only two teams, red or blue. Object depicting for symbolizing competing teams individually.

\subsubsection{Player}
Object meant for depicting the player. It is used for storing the information about the player and their current state in the game (their interaction with pieces, the board, etc.)

\subsubsection{Piece}
Object meant for depicting the piece. The current state of the piece (they could be carried by the player, on board undiscovered, etc.)

\subsubsection{Goal}
Object meant for depicting the Goal. Information about where the current goal is on the board, as well as allows the player to interact with it.



\subsection{Game Interactions}
The communication between the players should be possible without the approval or the supervision of the Game Master. The players should be able to request things from other players as long as it's within the game rules. In order for the player to interact with the Game Master, they need to send a request to the Game Master and expect some kind of response. The communication will be asynchronous. The players should always check the ``legitimacy" of what they see in the game with the Game Master, once they have confirmed the state of the program, they can be expected to continue playing. The players can exchange information about what they've discovered while playing the game (e.g. sham pieces, goal advancement, etc.)
\newpage
\subsection{Communication protocol}
All the messages are sent using HTTP protocol with POST method.
\subsubsection{Basic structure of messages}
All requests and responses will be structured in a similar way, leading to clarity when the protocol is applied.
\paragraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "<action name>",
	"<parameter name>": "<parameter value>",
	...
}
\end{lstlisting}
Every request identifies its intent through \texttt{action} field. It then can pass various parameters relating to this action.
\paragraph*{Response}\mbox{}\\

\begin{lstlisting}[language=json]
{
	"action": "<action name>",
	"response": "<either OK or denied>",
	"<parameter name>": "<parameter value>",
	...

}
\end{lstlisting}
The response relates to the request that was made by its action name and potentially parameters shared between them. The response returns a type of response that was issued: either action ended successfully (OK) or it was not (denied).
\paragraph*{Request-and-response relay}
Unless specified, an action is relayed by a communication server to a user responsible for a given action. Likewise, response to an action is relayed by a communication server to requesting user.
\newpage

\subsubsection{Actions}
In this project, there are some situations in which there is a need of communication between actors (i.e. game master, board, players). These situations are called \textit{actions}. Each action is a single-purpose communication between two actors (e.g. game master-player or player-player), conducted using communication server.

Actions are split into following categories:
\begin{itemize}
    \item \textbf{Game establishment} - actions that take part before the start of the game in order to prepare it, e.g. connecting players or setting up a board.
    \item \textbf{Game management} - actions that control the game during its course, e.g. checking the state of the board or resetting the game.
    \item \textbf{Player actions} - actions that can be taken by a Player during the game, e.g. moving around the board or picking up pieces.
    \item \textbf{Message handling} - actions that relate to communication between two Players, e.g. sending a message or accepting requests for communication.
    \item \textbf{State announcements} - actions that relate to the state in which the game is, e.g. the game is starting or there is a winner.
    \item \textbf{Warnings and errors} - actions that are sent whenever there is a problem with the game or the game can no longer continue.
\end{itemize}
\newpage
\subsubsection{List of actions}
\paragraph{Game establishment}
\subparagraph*{Connect to the game}
\subparagraph*{Request}\mbox{}\\\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "connect",
	"type": "<player or gamemaster or board>"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "connect",
	"response": "<either OK or denied>",
	"type": "<player or gamemaster or board>"
}
\end{lstlisting}
This action allows users to connect to the game. In request, a type of user is provided in order for communication server to decide which role to give to a user. In response, the ID of a user is returned, as well as either a team they belong to (if they're a player) or a role they've been assigned (if they're a game master or a board).

Connection may be denied if game master or board are already connected or game master decides a player cannot be accepted.
\subparagraph*{Set up game rules}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "setting",
	"name": "<name of the setting>",
	"value": "<value of the setting>"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "setting",
	"result": "<either OK or denied>",
	"name": "<name of the setting>"
}
\end{lstlisting}
This action allows a game master to set up the game - these settings may refer to size of the board (settings \texttt{X} and \texttt{Y}) as well as may include conditions on infinite loop detection and policies on number of active users (these are additional settings and may be provided by a given communication server.

This action may be denied due to board not being connected yet, setting not existing or against board's rules or because the request is not sent by a game master. In that case, a warning explaining the problem is sent alongside the response.
\subparagraph*{Set up players}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "player",
	"server_id": <server-established ID>
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "player",
	"result": "<either OK or denied>",
	"server_id": <server-established ID>,
	"position": <player position, e.g. {X:2, Y:5}>,
	"id": <game-master specific ID>,
	"team": "<team name>
}
\end{lstlisting}

When a user connects to communication server, the server relays that information to game master, who then decides on whether to allow the player, what is their position, their ID in the team and what their team is. This data is then later relayed to user when the game starts.
\paragraph{Game management}
\subparagraph*{Start the game}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "start"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "start",
	"result": "<either OK or denied>"
}
\end{lstlisting}
This action allows a game master to start a game. 

This action may be denied due to board not being connected yet or request not being sent by a game master. In that case, a warning explaining the problem is sent alongside the response.

This action also sends a respective state announcement to all users.
\subparagraph*{Check state of the board}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "state",
	"scope": "all"/{X:2, Y:5}
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "state",
	"result": "<either OK or denied>",
	"scope": "all"/{X:2, Y:5},
	"fields": [{"X":2, "Y":5, "type": "<either empty or sham or player or goal>", "id": "<id of a player>"}, ...]
}
\end{lstlisting}
This action allows a game master to check the state of the board. Game master may ask for state of all fields of the board or for state of a single field. In response, a game master receives the description of specified fields, including their position, type and ID of a player, if a player is on the field.

This action may be denied due to board not being connected yet or request not being sent by a game master. In that case, a warning explaining the problem is sent alongside the response.

\subparagraph*{Update the board}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "update",
	"position": {"X":2, "Y":5},
	"type": "<either empty or sham or player or goal>",
	"id":"<id of a player>"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action: "update",
	"result": "<either OK or denied>",
	"position": {"X":2, "Y":5}
}
\end{lstlisting}
This action allows a game master to update the state of the board. Game master may update a state of a given position by providing its new type and ID (in case a player was selected).

This action may be denied due to board not being connected yet, request not being sent by a game master or the field being already filled. In that case, a warning explaining the problem is sent alongside the response.
\subparagraph*{Announce the result}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "announce",
	"winner": "<name of team that won>"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "announce",
	"result": "<either OK or denied>"
}
\end{lstlisting}
This action allows a game master to announce who won the game. Winning the game means the game shuts down - all users disconnect from communication server.

This action may be denied due to request not being sent by a game master. 

This action also sends a respective state announcement to all users.

\subparagraph*{Reset the game}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "reset"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "reset",
	"result": "<either OK or denied>"
}
\end{lstlisting}
This action allows a game master to reset the game. In this situation, the game is stopped and the state of game is reset.

This action may be denied due to request not being sent by a game master. 

This action also sends a respective state announcement to all users.
\paragraph{Player actions}
\subparagraph*{Move in a given direction}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "move",
	"direction": "<(N/E/S/W)>"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "move",
	"result": "<either OK or denied>",
	"surroundings": ["2","8", "T", ... ]
}
\end{lstlisting}
This action allows a player to move in a given direction. Directions are given in a geographical sense, e.g. to move upwards a player has to move north, or \texttt{N}. In response, a player receives an array of eight elements, from top-left to bottom-right, moving rightwards, describing surroundings of position the player is currently in. The array may contain a Manhattan distance to another piece or a letter signifying a teammate (\texttt{T}), a player from opposite team (\texttt{O}), a piece (\texttt{P}), a goal spot (\texttt{G}) and a border of the board (\texttt{B}).

This action may be denied if the user tries to move outside of the board or another player is on the field. In that case, a warning explaining the problem is sent alongside the response.
\subparagraph*{Test a piece}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "test"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "test",
	"result": "<either OK or denied>",
	"test": "<either true or false or null>"
}
\end{lstlisting}
This action allows a player to test if a piece they are on is a sham. If it is a sham, the \texttt{test} field in response will return \texttt{true}. If it's not, the value of that field will be \texttt{false}. If no piece is being tested, the value will be \texttt{null}.
\newpage
\subparagraph*{Pick up a piece}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "pickup"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "pickup",
	"result": "<either OK or denied>"
}
\end{lstlisting}
This action allows a player to pick up a piece.

This action may be denied if a player tries to pick up a piece where it doesn't exist.
\subparagraph*{Leave a piece}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "leave"
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "leave",
	"result": "<either OK or denied>",
	"consequence": "<either completed or meaningless or none>"
}
\end{lstlisting}
This action allows a player to leave a piece. In response, the player can find out whether the consequence of this action was: completion of the task (\texttt{completed}), the action had no meaning (\texttt{meaningless}) or that there is no consequence, which means the piece was a sham (\texttt{none}).

This action may be denied if a player tries to leave a piece they don't carry.
\newpage
\paragraph{Message handling}
\subparagraph*{Send a message}
\subparagraph*{Request}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "send",
	"to": <player id>,
	"fields": [{"X":2, "Y":5, "value": "2"}, ...] 
}
\end{lstlisting}
\subparagraph*{Response}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "send",
	"result": "<either OK or denied>"
}
\end{lstlisting}
This action allows a player to send a message to another user on the team, as specified by its ID number. In the message, a player may send an array of descriptions of fields: their location and their value.

This action may be denied if the other user has denied the connection.
\subparagraph*{Accept or deny a request from message sender}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "request",
	"from": <sender id>
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "request",
	"request": "<either OK or denied>",
	"from": <sender id>
}
\end{lstlisting}
In case a player tries to send a message to another player, the communication server sends a request to that player with information which player tries to establish communication. In response, a player sends an information whether they accept or deny data exchanges in the future. 

This request is sent only once: if a player denies, it cannot accept it again. This request is not sent when a team leader tries to send a message - it's automatically sent, without any need for approval.
\subparagraph*{Receive the message}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "message",
	"from": <sender ID>,
	"fields": [{"X":2, "Y":5, "value": "2"}, ...]
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "message",
	"result": "OK",
	"from": <sender ID> 
}
\end{lstlisting}
In case communication has been allowed (or a team leader sends a message), the communication server sends a request to the player, relaying the message from sending player. The receiving player must acknowledge the message has been received.
\newpage
\paragraph{State announcements}
\subparagraph*{The game is starting}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action":"start",
	"team": "<team name>",
	"id": <player ID>,
	"location": <player location, e.g. {X:2, Y:5}>
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "start",
	"result": "OK"
}
\end{lstlisting}
When the game starts, the communication server sends an information to all players about that fact. In this request, information about team name, player ID and location is relayed. The player must acknowledge this message.
\subparagraph*{The game has been reset}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "reset"
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "reset",
	"result": "OK"
}
\end{lstlisting}
When the game resets, it loses all data it already has and establishes itself again as if it was ran for the first time. In this situation, the player is disconnected from the communication server after acknowledging the message and must connect again in order to participate in new game.
\newpage
\subparagraph*{The game has a winner}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "announce",
	"winner": "<team name>"
}
\end{lstlisting}
\subparagraph*{Response to server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "announce",
	"result": "OK"
}
\end{lstlisting}
When the game has a winner, it is being announced to all players. In this situation, the player is disconnected from communication server after acknowledging the message and shuts down, as presumably the game won't start again.
\newpage
\paragraph{Warnings and errors}
\subparagraph*{Warnings}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "warning",
	"name": "InfiniteLoopWarning",
	"message": "An infinite loop has been detected by Board."
}
\end{lstlisting}
Warnings are sent in situations where something bad is happening, but there is an action that can be taken by a game master in order to recover the game. 

Warnings do not require response to the server.
\subparagraph*{Errors}
\subparagraph*{Request from server}\mbox{}\\
\begin{lstlisting}[language=json]
{
	"action": "error",
	"type": "BoardDisconnectedError",
	"message": "Communication server has lost connection to Board."
}
\end{lstlisting}
Errors are sent in situations where something bad has happened and it's impossible for the game to recover. When an error is sent, it means that parts of the game need to shut down, as they're no longer needed - the game is already over. 

Errors do not require response to the server.
\subsection{Diagrams}

\subsubsection{Use case diagrams}

\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{UseCase1.pdf}}
\end{center}
\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{UseCase2.pdf}}
\end{center}

\subsubsection{Class diagrams}

\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{ClassDiagram1.pdf}}
\end{center}

\subsubsection{State diagrams}

\begin{center}
  \makebox[\textwidth]{\includegraphics[height=\paperheight,width=\paperwidth]{PlayerState.pdf}}
\end{center}
\includepdf{StateDiagram1.pdf}




\subsubsection{Activity diagrams}

\begin{center}
  \makebox[\textwidth]{\includegraphics[scale=0.9]{GameActivity.pdf}}
\end{center}
\includepdf{Activity1.pdf}

\subsubsection{Sequence diagrams}

\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{Diagram1.pdf}}
\end{center}
\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{Diagram2.pdf}}
\end{center}
\begin{center}
  \makebox[\textwidth]{\includegraphics[width=\paperwidth]{Diagram3.pdf}}
\end{center}
\newpage
\section{Glossary}
\begin{itemize}
\item\textbf{Project Game} - a real-time board game played by a two competing teams of
cooperating players
\item\textbf{Game Master} - sets up the \textit{Board}, passes moves from \textit{players} and declares the winner of the game
\item\textbf{Board} - holds
the real state of the game as defined by \textit{pieces}, \textit{goals} and \textit{players} on it  
\item\textbf{Player} - an agent playing the game, holds its own view of the state of the game
\item\textbf{Communication Server} - responsible for passing messages between \textit{Game
Master}, \textit{Board} and \textit{Players}
\item\textbf{Team} - group of \textit{Players} who cooperate in order to achieve the goal of the
game
\item\textbf{Goal Area} - a part of the board where the \textit{Players} place the \textit{pieces}
\item\textbf{Tasks Area} - a part of the board from which the \textit{Players} may collect the \textit{pieces}
\item\textbf{Game Goal} - discovering the shape of the project goal by placing a set of
pieces in the \textit{Goal Area}
\item\textbf{Piece} - a token representing the project resource which is placed in the \textit{Tasks
Area} of the board by the \textit{Game Master}, picked from the \textit{Tasks Area} by
one of the \textit{Players} and placed by that \textit{Player} in the \textit{Goal Area}
\item\textbf{Action} - a single-purpose communication between two actors (e.g. game master-player or player-player), conducted using Communication Server.
\end{itemize}
\end{document}